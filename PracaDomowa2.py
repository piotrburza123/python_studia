# Slajd 37
# Wypisanie imienia
def moje_imie() -> None:
    print("Piotr")
moje_imie()

########################################
# Suma trzech liczb
def suma_liczb(a, b, c) -> None:
    print(a + b + c)
suma_liczb(3, 3, 3)

########################################
# Hello World

def hello_world() -> str:
    hello = "Hello World!"
    return hello
print(hello_world())

########################################
# Odejmowanie

def odejmowanie(a, b) -> int:
    return a - b
print(odejmowanie(13, 8))

# Slajd 46
class A:
    def __init__(self):
        self.a = 1
        self.b = 2

class B(A):
    pass

object_b = B()
print(object_b.a, object_b.b)
object_b.a = 3
object_b.b = 4
print(object_b.a, object_b.b)

########################################

class C:
    def __init__(self):
        self.string_to_hello_function = "Metoda z klasy C"

    def hello(self) -> None:
        print(self.string_to_hello_function)

class D(C):
    pass

object_d = D()
object_d.hello()






